<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
	<meta name="description" content="Modern admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities with bitcoin dashboard.">
	<meta name="keywords" content="admin template, modern admin template, dashboard template, flat admin template, responsive admin template, web app, crypto dashboard, bitcoin dashboard">
	<meta name="author" content="PIXINVENT">
	<title>To-Do - Modern Admin - Clean Bootstrap 4 Dashboard HTML Template + Bitcoin Dashboard</title>
	<link rel="apple-touch-icon" href="<?= base_url('public/app-assets/images/ico/apple-icon-120.png') ?>">
	<link rel="shortcut icon" type="image/x-icon" href="<?= base_url('public/app-assets/images/ico/favicon.ico') ?>">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i%7CQuicksand:300,400,500,700" rel="stylesheet">

	<!-- BEGIN: Vendor CSS-->
	<link rel="stylesheet" type="text/css" href="<?= base_url('public/app-assets/vendors/css/vendors.min.css') ?>">
	<link rel="stylesheet" type="text/css" href="<?= base_url('public/app-assets/vendors/css/tables/datatable/datatables.min.css') ?>">
	<link rel="stylesheet" type="text/css" href="<?= base_url('public/app-assets/vendors/css/pickers/daterange/daterangepicker.css') ?>">
	<link rel="stylesheet" type="text/css" href="<?= base_url('public/app-assets/vendors/css/forms/selects/select2.min.css') ?>">
	<link rel="stylesheet" type="text/css" href="<?= base_url('public/app-assets/vendors/css/editors/quill/quill.snow.css') ?>">
	<link rel="stylesheet" type="text/css" href="<?= base_url('public/app-assets/vendors/css/extensions/dragula.min.css') ?>">
	<!-- END: Vendor CSS-->

	<!-- BEGIN: Theme CSS-->
	<link rel="stylesheet" type="text/css" href="<?= base_url('public/app-assets/css/bootstrap.css') ?>">
	<link rel="stylesheet" type="text/css" href="<?= base_url('public/app-assets/css/bootstrap-extended.css') ?>">
	<link rel="stylesheet" type="text/css" href="<?= base_url('public/app-assets/css/colors.css') ?>">
	<link rel="stylesheet" type="text/css" href="<?= base_url('public/app-assets/css/components.css') ?>">
	<!-- END: Theme CSS-->

	<!-- BEGIN: Page CSS-->
	<link rel="stylesheet" type="text/css" href="<?= base_url('public/app-assets/css/core/menu/menu-types/vertical-menu-modern.css') ?>">
	<link rel="stylesheet" type="text/css" href="<?= base_url('public/app-assets/css/core/colors/palette-gradient.css') ?>">
	<link rel="stylesheet" type="text/css" href="<?= base_url('public/app-assets/css/pages/app-todo.css') ?>">
	<!-- END: Page CSS-->

	<!-- BEGIN: Custom CSS-->
	<link rel="stylesheet" type="text/css" href="../../../assets/css/style.css">
	<!-- END: Custom CSS-->

</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern content-left-sidebar todo-application  fixed-navbar" data-open="click" data-menu="vertical-menu-modern" data-col="content-left-sidebar">

	<!-- BEGIN: Header-->
	<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu navbar-without-dd-arrow fixed-top navbar-semi-dark navbar-shadow">
		<div class="navbar-wrapper">
			<div class="navbar-header">
				<ul class="nav navbar-nav flex-row">
					<li class="nav-item mobile-menu d-lg-none mr-auto"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu font-large-1"></i></a></li>
					<li class="nav-item mr-auto"><a class="navbar-brand" href="<?=site_url()?>"><img class="brand-logo" alt="modern admin logo" src="<?= base_url('public/app-assets/images/logo/logo.png') ?>">
							<h3 class="brand-text">AdminVet</h3>
						</a></li>
					<li class="nav-item d-none d-lg-block nav-toggle"><a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><i class="toggle-icon ft-toggle-right font-medium-3 white" data-ticon="ft-toggle-right"></i></a></li>
					<li class="nav-item d-lg-none"><a class="nav-link open-navbar-container" data-toggle="collapse" data-target="#navbar-mobile"><i class="la la-ellipsis-v"></i></a></li>
				</ul>
			</div>
			<div class="navbar-container content">
				<div class="collapse navbar-collapse" id="navbar-mobile">
					<ul class="nav navbar-nav mr-auto float-left">
						<li class="nav-item d-none d-lg-block"><a class="nav-link nav-link-expand" href="#"><i class="ficon ft-maximize"></i></a></li>
					</ul>
					<ul class="nav navbar-nav float-right">
						<li class="dropdown dropdown-user nav-item"><a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown"><span class="mr-1 user-name text-bold-700">John Doe</span><span class="avatar avatar-online"><img src="<?= base_url('public/app-assets/images/portrait/small/avatar-s-19.png') ?>" alt="avatar"><i></i></span></a>
							<div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="user-profile.html"><i class="ft-user"></i> Editar Usuario</a>
								<div class="dropdown-divider"></div><a class="dropdown-item" href="login-with-bg-image.html"><i class="ft-power"></i> Salir</a>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</nav>
	<!-- END: Header-->


	<!-- BEGIN: Main Menu-->

	<div class="main-menu menu-fixed menu-dark menu-accordion menu-shadow" data-scroll-to-active="true">
		<div class="main-menu-content">
			<ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
				<li class=" navigation-header"><span data-i18n="Admin Panels">Propietarios</span><i class="la la-ellipsis-h" data-toggle="tooltip" data-placement="right" data-original-title="Admin Panels"></i>
				</li>
				<li class=" nav-item"><a href="../ecommerce-menu-template" target="_blank"><i class="la la-users"></i><span class="menu-title" data-i18n="eCommerce">Propietarios</span></a>
				</li>
			</ul>
		</div>
	</div>

	<!-- END: Main Menu-->
	<!-- BEGIN: Content-->
	<?= $this->renderSection("content"); ?>
    
    <!-- END: Content-->

	<div class="sidenav-overlay"></div>
	<div class="drag-target"></div>

	<!-- BEGIN: Footer-->
	<footer class="footer footer-static footer-light navbar-border navbar-shadow">
		<p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2"><span class="float-md-left d-block d-md-inline-block">Copyright &copy; 2019 <a class="text-bold-800 grey darken-2" href="https://1.envato.market/modern_admin" target="_blank">PIXINVENT</a></span><span class="float-md-right d-none d-lg-block">Hand-crafted & Made with<i class="ft-heart pink"></i><span id="scroll-top"></span></span></p>
	</footer>
	<!-- END: Footer-->


	<!-- BEGIN: Vendor JS-->
	<script src="<?= base_url('public/app-assets/vendors/js/vendors.min.js') ?>"></script>
	<!-- BEGIN Vendor JS-->

	<!-- BEGIN: Page Vendor JS-->
	<script src="<?= base_url('public/app-assets/vendors/js/pickers/daterange/moment.min.js') ?>"></script>
	<script src="<?= base_url('public/app-assets/vendors/js/pickers/daterange/daterangepicker.js') ?>"></script>
	<script src="<?= base_url('public/app-assets/vendors/js/forms/select/select2.full.min.js') ?>"></script>
	<script src="<?= base_url('public/app-assets/vendors/js/editors/quill/quill.min.js') ?>"></script>
	<script src="<?= base_url('public/app-assets/vendors/js/extensions/dragula.min.js') ?>"></script>
	<script src="<?= base_url('public/app-assets/vendors/js/tables/datatable/datatables.min.js') ?>"></script>
	<!-- END: Page Vendor JS-->

	<!-- BEGIN: Theme JS-->
	<script src="<?= base_url('public/app-assets/js/core/app-menu.js') ?>"></script>
	<script src="<?= base_url('public/app-assets/js/core/app.js') ?>"></script>
	<!-- END: Theme JS-->

	<!-- BEGIN: Page JS-->
	<script src="<?= base_url('public/app-assets/js/scripts/pages/app-todo.js') ?>"></script>
	<script src="<?= base_url('public/app-assets/js/scripts/tables/datatables/datatable-basic.js') ?>"></script>
	<?= $this->renderSection("scripts"); ?>
	<!-- END: Page JS-->

	
</body>
<!-- END: Body-->

</html>