<?php

namespace App\Models;

use CodeIgniter\Model;

class PropietarioModel extends Model
{
    protected $table      = 'propietario';
    protected $primaryKey = 'idPropietario';

}
